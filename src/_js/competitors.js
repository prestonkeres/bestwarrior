/*global $, document*/
$(document).ready(function() {
    var filter, bios = $('ul.bios li'), i;
    $('#filter li').click(function() {
        $('#filter li.current').removeClass();
        $(this).addClass('current');

        filter = $(this).text();

        if (filter == 'ALL') {
            bios.removeClass('hidden');
        } else {
            for (i = 0; i < bios.length; i++) {
                if(!$(bios[i]).hasClass(filter)) {
                    $(bios[i]).addClass('hidden');
                } else {
                    $(bios[i]).removeClass('hidden');
                }
            }
        }
        return false;
    });
});
